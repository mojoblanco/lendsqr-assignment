const bcrypt = require('bcrypt');

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  const user = await knex('users').first();

  if (user) return;

  const password = await bcrypt.hash('password', 10);

  await knex.transaction(async (trx) => {
    await knex('users').insert([
      {
        first_name: 'Mason',
        last_name: 'Mount',
        email: 'mason@mount.com',
        password
      },
      {
        first_name: 'Bruce',
        last_name: 'Wayne',
        email: 'bruce@wayne.com',
        password
      },
    ]);

    const users = await knex('users').select('*');

    for (let i = 0; i < users.length; i++) {
      await knex('accounts').insert([
        {
          balance: 0,
          account_type: 'savings',
          user_id: users[i].id
        }
      ]);
    }
  });
};
