/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable("transactions", function (table) {
        table.bigIncrements('id').unsigned().primary();
        table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
        table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
        table.decimal('amount', 10, 2).notNullable();
        table.bigInteger('main_account_id').unsigned().index().references('id').inTable('accounts').notNullable();
        table.bigInteger('sub_account_id').unsigned().index().references('id').inTable('accounts').nullable();
        table.string('type', 255).notNullable();
        table.string('details', 255).nullable();
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable("transactions");
};
