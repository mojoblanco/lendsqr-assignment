/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable("accounts", function (table) {
        table.bigIncrements('id').unsigned().primary();
        table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
        table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
        table.decimal('balance', 10, 2).notNullable();
        table.string('account_type', 255).notNullable();
        table.bigInteger('user_id').unsigned().index().references('id').inTable('users').notNullable();
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable("accounts");
};
