const knex = require('knex');
const knexStringcase = require('knex-stringcase');

const dbConfig = {
    client: 'mysql',
    connection: {
        host : process.env.DB_HOST,
        user : process.env.DB_USER,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME
    }
};

const dbOptions = knexStringcase(dbConfig);

exports.db = knex(dbOptions);
