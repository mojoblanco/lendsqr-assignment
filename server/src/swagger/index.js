exports.swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Ledsqr assignment API",
            description: "API documentation for Lendsqr assignment",
            version: '1.0.0',
        },
        basePath: '/',
    },
    apis: ['./src/http/controllers/*.js']
};