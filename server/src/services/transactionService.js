const { db } = require('../db/knex');
const { ErrorHandler } = require('../helpers/ErrorHandler');

exports.insertFund = async (accountId, amount) => {
    if (amount <= 0) throw new ErrorHandler(400, 'Amount is invalid');

    const account = await db('accounts').where({ id: accountId }).first();
    if (!account)
        throw new ErrorHandler(404, 'Account not found');

    await db.transaction(async (trx) => {
        await trx('transactions')
            .insert({
                amount,
                mainAccountId: accountId,
                type: 'credit',
                details: 'Fund Account'
            });

        await trx('accounts')
            .where({ id: accountId })
            .update({ balance: account.balance + parseFloat(amount) });
    });
}

exports.transferFund = async (accountId, beneficiaryEmail, amount) => {
    if (amount <= 0) throw new ErrorHandler(400, 'Amount is invalid');

    const mainAccount = await db('accounts')
        .join('users', 'users.id', 'accounts.user_id')
        .where('accounts.id', accountId)
        .select('accounts.id', 'accounts.balance', 'users.email')
        .first();
    if (!mainAccount)
        throw new ErrorHandler(404, 'Account not found');
    if (mainAccount.balance < amount) throw new ErrorHandler(400, 'Insufficient funds');

    const subAccount = await db('accounts')
        .join('users', 'users.id', 'accounts.user_id')
        .where('users.email', beneficiaryEmail)
        .select('accounts.id', 'accounts.balance', 'users.email')
        .first();
    if (!subAccount)
        throw new ErrorHandler(404, 'Beneficary account not found');
    if (subAccount.email == mainAccount.email)
        throw new ErrorHandler(400, 'You cannot transfer to yourself');

    await db.transaction(async (trx) => {
        await trx('transactions')
            .insert({
                amount,
                mainAccountId: accountId,
                subAccountId: subAccount.id,
                type: 'debit',
                details: 'Transfer'
            });

        await trx('transactions')
            .insert({
                amount,
                mainAccountId: subAccount.id,
                type: 'credit',
                details: 'Transfer'
            });

        await trx('accounts')
            .where({ id: mainAccount.id })
            .update({ balance: mainAccount.balance - parseFloat(amount) });

        await trx('accounts')
            .where({ id: subAccount.id })
            .update({ balance: subAccount.balance + parseFloat(amount) });
    });
}

exports.withdrawFund = async (accountId, amount, accountNumber) => {
    if (amount <= 0) throw new ErrorHandler(400, 'Amount is invalid');

    const account = await db('accounts').where({ id: accountId }).first();
    if (!account)
        throw new ErrorHandler(404, 'Account not found');
    if (account.balance < amount) throw new ErrorHandler(400, 'Insufficient funds');

    await db.transaction(async (trx) => {
        await trx('transactions')
            .insert({
                amount,
                mainAccountId: accountId,
                type: 'debit',
                details: 'Withdrawal'
            });

        await trx('accounts')
            .where({ id: accountId })
            .update({ balance: account.balance - parseFloat(amount) });
    });

    console.log(`Withdrawal to bank account: ${accountNumber} was successful`);
}