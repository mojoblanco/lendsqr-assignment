const { db } = require('../db/knex');
const bcrypt = require('bcrypt');
const { ErrorHandler } = require('../helpers/ErrorHandler');

exports.getSession = async () => {
    // Use the first user for all logins
    const user = await db('users')
        .join('accounts', 'users.id', 'accounts.user_id')
        .orderBy('users.created_at', 'asc')
        .select('users.first_name', 'users.last_name', 'users.email', 'users.id as userId', 'accounts.id as accountId', 'accounts.balance')
        .first();

    const { password, ...data } = user;

    return data;
}

exports.createUser = async (userData) => {
    const { email, password, firstName, lastName } = userData;

    const existingUser = await db('users').where({ email }).first();

    if (existingUser)
       throw new ErrorHandler(400, 'User already exists');

    const hashedPassword = await bcrypt.hash(password, 10);
    await db.transaction(async (trx) => {
        const user = await trx('users')
            .insert({
                email,
                password: hashedPassword,
                firstName,
                lastName
            });

        await trx('accounts').insert([
            {
                balance: 0,
                account_type: 'savings',
                user_id: user[0]
            }
        ]);
    });
}