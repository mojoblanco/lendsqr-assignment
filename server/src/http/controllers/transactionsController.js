const { insertFund, transferFund, withdrawFund } = require('../../services/transactionService');


/**
 * @swagger
 * /fund:
 *   post:
 *     description: Fund Account
 *     tags:
 *       - Transactions
 *     parameters:
 *      - name: amount
 *        description: Amount to fund
 *        in: formData
 *        required: true
 *        type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.fundAccount = async (req, res, next) => {
    try {
        const { amount, accountId } = req.body;

        await insertFund(accountId, amount);

        return res.status(200).json({
            message: 'Account funding successful',
        });
    } catch (error) {
        next(error);
    }
};


/**
 * @swagger
 * /transfer:
 *   post:
 *     description: Transfer funds
 *     tags:
 *       - Transactions
 *     parameters:
 *      - name: email
 *        description: Email address
 *        in: formData
 *        required: true
 *        type: string
 *      - name: amount
 *        description: Amount to transfer
 *        in: formData
 *        required: true
 *        type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.transferFund = async (req, res, next) => {
    try {
        const { amount, accountId, email } = req.body;
        await transferFund(accountId, email, amount);

        return res.status(200).json({
            message: 'Transfer was successful'
        });
    } catch (error) {
        next(error);
    }
};


/**
 * @swagger
 * /withdraw:
 *   post:
 *     description: Withdraw funds
 *     tags:
 *       - Transactions
 *     parameters:
 *      - name: amount
 *        description: Amount to withdraw
 *        in: formData
 *        required: true
 *        type: number
 *      - name: accountNumber
 *        description: Account Number
 *        in: formData
 *        required: true
 *        type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.withdrawFund = async (req, res, next) => {
    try {
        const { amount, accountId, accountNumber } = req.body;
        await withdrawFund(accountId, amount, accountNumber);

        return res.status(200).json({
            message: 'Withdrawal was successful'
        });
    } catch (error) {
        next(error);
    }
};
