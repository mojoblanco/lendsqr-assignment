const { createUser, getSession } = require('../../services/authService');


/**
 * @swagger
 * /session:
 *   get:
 *     description: User session
 *     tags:
 *       - Auth
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.session = async (req, res, next) => {
    const data = await getSession();

    return res.status(200).json({ data })
}

/**
 * @swagger
 * /login:
 *   post:
 *     description: Login
 *     tags:
 *       - Auth
 *     parameters:
 *      - name: email
 *        description: Email address
 *        in: formData
 *        required: true
 *        type: string
 *      - name: password
 *        description: Password
 *        in: formData
 *        required: true
 *        type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.login = async (req, res, next) => {
    return res.status(200).json({
        message: 'Login successful',
        data: { token: 'abcdefgh' }
    });
};


/**
 * @swagger
 * /register:
 *   post:
 *     description: Register
 *     tags:
 *       - Auth
 *     parameters:
 *      - name: firstNname
 *        description: First Name
 *        in: formData
 *        required: true
 *        type: string
 *      - name: lastName
 *        description: Last Name
 *        in: formData
 *        required: true
 *        type: string
 *      - name: email
 *        description: Email address
 *        in: formData
 *        required: true
 *        type: string
 *      - name: password
 *        description: Password
 *        in: formData
 *        required: true
 *        type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Bad request
 *       401:
 *         description: Unauthorized
 */
exports.register = async (req, res, next) => {
    try {
        await createUser(req.body);

        return res.status(200).json({
            message: 'Registration was successful'
        });
    } catch (error) {
        next(error);
    }
};

