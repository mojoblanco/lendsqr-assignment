var express = require('express');
var router = express.Router();
const { login, register, session } = require('../http/controllers/authController');
const { fundAccount, transferFund, withdrawFund } = require('../http/controllers/transactionsController');
const { handleError } = require('../helpers/ErrorHandler');

router.get('/', (req, res, next) => {
    return res.json('Hello');
});

router.get('/session', session);
router.post('/login', login);
router.post('/register', register);

router.post('/fund', fundAccount);
router.post('/transfer', transferFund);
router.post('/withdraw', withdrawFund);

router.use((err, req, res, next) => {
    handleError(err, res);
});

module.exports = router;
