require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require('swagger-ui-express');
const { swaggerOptions } = require('./swagger');
const path = require('path');
const cors = require('cors');

var app = express();

app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/', require('./routes/indexRoute'));

if (process.env.APP_ENV !== 'production') {
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use('/apidocs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));
}

app.get('*', function (req, res) {
  res.status(404).send('<h1>404. Page not found</h1>');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
