class ErrorHandler extends Error {
    constructor(statusCode, message, errors = null) {
        super();
        this.statusCode = statusCode;
        this.message = message;
        this.errors = errors;
    }
}

const handleError = (err, res) => {
    const { response, statusCode, message, errors } = err;

    if (response && response.status === 500) {
        return res.status(500).json({ message: "Internal server error" });
    }

    if (response && response.data) {
        return res.status(response.status).json(response.data);
    }

    let errorResponse = { status: 'error', message };
    if (errors) errorResponse.errors = errors;

    if (!statusCode) {
        console.log(message);
        return res.status(500).json({ message: "An unexpected error occured" });
    }

    return res.status(statusCode).json(errorResponse);
}

module.exports = {
    ErrorHandler,
    handleError
}