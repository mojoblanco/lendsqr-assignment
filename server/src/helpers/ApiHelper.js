const { default: axios } = require("axios");
const { logger } = require('./Logger');

class ApiHelper {
    async getRequest(url, params = null, headers = null) {
        try {
            const { data } = await axios.get(url, { params, headers });
            return { data };
        } catch (error) {
            logger.error(`API_ERROR: ${url} - ${error.message}`);
            return { error: error.message };
        }
    }
}

module.exports = new ApiHelper;