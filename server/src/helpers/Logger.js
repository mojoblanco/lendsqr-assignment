
const winston = require('winston');
const DailyRotateFile  = require('winston-daily-rotate-file');
const appRoot = require('app-root-path');
const AppLog = require('../db/models/AppLog');

// Setup logger
const logger = winston.createLogger({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json()
    ),
    transports: [
      new DailyRotateFile  ({
        filename:  'error-%DATE%.log',
        dirname:  `${appRoot}/logs/`,
        level:  'error',
        handleExceptions:  true,
        colorize:  true,
        json:  false,
        zippedArchive:  true,
        maxSize:  '20m',
        maxFiles: '14d',
      }),
      new DailyRotateFile({
        filename: 'combined-%DATE%.log',
        dirname: `${appRoot}/logs/`,
        level: 'info',
        handleExceptions: true,
        colorize: true,
        json: false,
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d'
      })
    ],exitOnError:  false});

const dbLog = async (level, label, message = "", identifier = "", data = "") => {
  let appLog = new AppLog();
  appLog.level = level;
  appLog.label = label;
  appLog.message = message;
  appLog.identifier = identifier;
  appLog.data = JSON.stringify(data);

  await appLog.save();
}

module.exports = {
  logger,
  dbLog
}
