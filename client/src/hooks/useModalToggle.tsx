import {useState} from 'react';

export default (initialState: boolean = false) => {
  const [open, setOpen] = useState(initialState);

  const handleToggle = () => {
    setOpen(!open);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return {
    handleToggle,
    handleClose,
    handleOpen,
    open,
  };
};
