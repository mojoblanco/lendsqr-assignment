import {FC, useEffect, useState} from "react";
import { Link } from "react-router-dom";
import { FundAccountModal, TransferFundsModal, WithdrawFundsModal } from "../conponents/modals"
import useModalToggle from "../hooks/useModalToggle"
import { getSession } from "../helpers/apiHelper"

const sections = [
  {
    title: 'Fund account',
    description: 'Fund your account',
    action: 'fund-account'
  },
  {
    title: 'Transfer funds',
    description: 'Transfer funds from your account to another',
    action: 'transfer-funds'
  },
  {
    title: 'Withdraw funds',
    description: 'Withdraw funds from your account',
    action: 'withdraw-funds'
  },
]

const Home: FC = () => {
  const {
    open: showFundAccountModal,
    handleOpen: openFundAccountModal,
    handleClose: closeFundAccountModal,
  } = useModalToggle(false)

  const {
    open: showTransferFundsModal,
    handleOpen: openTransferFundsModal,
    handleClose: closeTransferFundsModal,
  } = useModalToggle(false)

  const {
    open: showWithdrawFundsModal,
    handleOpen: openWithdrawFundsModal,
    handleClose: closeWithdrawFundsModal,
  } = useModalToggle(false)

  const [loading, setLoading] = useState({});
  const [userData, setUserdata] = useState({
    firstName: '',
    lastName: '',
    accountId: 0,
    balance: 0,
  });

  const fetchData = async () => {
    setLoading(true);
    const { error, data} = await getSession();

    error ? alert(error) : setUserdata(data.data);
    setLoading(false);
  }
  useEffect(async () => {
    await fetchData();
  }, []);


  const handleAction = (action: string) => {
    switch (action) {
      case 'fund-account':
        openFundAccountModal()
        break;
      case 'transfer-funds':
        openTransferFundsModal()
        break;
      case 'withdraw-funds':
        openWithdrawFundsModal()
        break;
      default:
        break;
    }
  }

  return (
    <>
      <div className="mb-8">
        <p className="text-2xl font-bold mb-2">
          Welome back, {userData.firstName} {userData.lastName}
        </p>
        <Link to='/login' className="font-semibold underline">
          Logout
        </Link>
      </div>

      {loading ?
        <p className="text-lg text-blue-500">Please wait...</p>
        :
        <div className="bg-white rounded shadow border p-6 mb-8">
          <p className="text-base uppercase mb-2 font-medium">
            Your current balance
          </p>
          <div className="font-bold text-2xl mb-4">
            NGN {userData.balance}
          </div>

          <div className="flex flex-col sm:flex-row">
            {sections.map(
              ({ title, action }) =>
                <p key={title} className="mb-2">
                  <Link
                    to="#"
                    className="text-blue-500 font-medium mr-4"
                    onClick={() => handleAction(action)}>
                    {title}
                  </Link>
                </p>
            )}
          </div>
        </div>
      }

      <FundAccountModal
        accountId={userData.accountId}
        isOpen={showFundAccountModal}
        closeModal={closeFundAccountModal}
        onSuccess={() => {
          alert('Successfully funded account');
          fetchData();
        }}
      />

      <TransferFundsModal
        isOpen={showTransferFundsModal}
        closeModal={closeTransferFundsModal}
        accountId={userData.accountId}
        onSuccess={() => {
          alert('Transfer was successful');
          fetchData();
        }}
      />

      <WithdrawFundsModal
        isOpen={showWithdrawFundsModal}
        closeModal={closeWithdrawFundsModal}
        accountId={userData.accountId}
        onSuccess={() => {
          alert('Withdrawal was successful');
          fetchData();
        }}
      />
    </>
  )
}

export default Home;
