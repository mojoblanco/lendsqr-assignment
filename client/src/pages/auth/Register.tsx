import { FC } from "react";
import { Link, useNavigate } from "react-router-dom";
import { XButton } from "../../conponents/ui";

const Register: FC = () => {
  const navigate = useNavigate()

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    navigate('/login')
  }

  return (
    <>
      <h1 className="text-3xl font-bold text-center mb-4">
        Lend<span className="text-gray-500">sqr</span>
      </h1>

      <div className="bg-white rounded shadow border p-6 mb-8 max-w-sm mx-auto">
        <h3 className='mb-2 font-bold text-lg'>
          Register
        </h3>
        <p className="text-sm mb-6">
          Fill in your details
        </p>

        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              htmlFor="first_name"
              className="text-gray-500"
            >
              First Name
            </label>
            <input
              id="first_name"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="last_name"
              className="text-gray-500"
            >
              Last Name
            </label>
            <input
              id="last_name"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="phone"
              className="text-gray-500"
            >
              Phone Numnber
            </label>
            <input
              id="phone"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="email"
              className="text-gray-500"
            >
              Email Address
            </label>
            <input
              id="email"
              type="email"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="email"
              className="text-gray-500"
            >
              Password
            </label>
            <input
              id="password"
              type="password"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="password_confirm"
              className="text-gray-500"
            >
              Confirm Password
            </label>
            <input
              id="password_confirm"
              type="password"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <XButton
            label="Register"
            onClick={() => { handleSubmit }}
          />
        </form>

        <p className="mt-4">
          Have an account? <Link to="/login" className="underline">Login here</Link>
        </p>
      </div>
    </>
  )
}

export default Register
