import { FC } from "react";
import { Link, useNavigate } from "react-router-dom";
import { XButton } from "../../conponents/ui";

const Login: FC = () => {
  const navigate = useNavigate()
  const handleLogin = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    navigate('/home')
  }

  return (
    <>
      <h1 className="text-3xl font-bold text-center mb-4">
        Lend<span className="text-gray-500">sqr</span>
      </h1>

      <div className="bg-white rounded shadow border p-6 mb-8 max-w-sm mx-auto">
        <h3 className='mb-2 font-bold text-lg'>
          Login
        </h3>
        <p className="text-sm mb-6">
          Enter your credentials
        </p>

        <form onSubmit={handleLogin}>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="text-gray-500"
            >
              Email Address
            </label>
            <input
              id="email"
              type="email"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="email"
              className="text-gray-500"
            >
              Password
            </label>
            <input
              id="password"
              type="password"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
            />
          </div>

          <XButton
            label="Login"
            onClick={() => { handleLogin }}
          />
        </form>

        <p className="mt-4">
          Don't have an account? <Link to="/register" className="underline">Register here</Link>
        </p>
      </div>
    </>
  )
}

export default Login
