import React, { FC, ReactChild, ReactChildren } from 'react'
import Navbar from './Navbar'

interface LayoutProps {
  children: ReactChild | ReactChildren
}

const Layout:FC<LayoutProps> = ({ children }) => {
  return (
    <div className='bg-gray-100 h-screen'>
      {/* <Navbar /> */}

      <div className='container mx-auto mt-8 p-8'>
        {children}
      </div>
    </div>
  )
}

export default Layout;
