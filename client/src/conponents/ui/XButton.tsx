import React, { FC } from 'react'
interface ButtonProps {
  onClick?: () => void | React.FormEvent<HTMLFormElement>
  label: string
  type?: 'button' | 'submit'
  loading? : boolean
}

const XButton: FC<ButtonProps> = ({
  onClick,
  label,
  type = 'submit',
  loading = false
}) => {
  return (
    <>
      <button
        type={type}
        className={`${loading ? 'bg-blue-200' : 'bg-blue-500'} text-white text-lg px-6 py-2 w-full`}
        disabled={loading}
      >
        {loading ? 'Please wait' : label }
      </button>
    </>
  )
}

export default XButton
