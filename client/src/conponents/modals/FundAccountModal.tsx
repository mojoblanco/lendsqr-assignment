import {FC, useState} from "react"
import Modal from "../Modal"
import { fundAccount } from "../../helpers/apiHelper"
import { XButton } from "../ui";

interface FundAccountModalProps {
  accountId: number,
  isOpen: boolean;
  closeModal: () => void;
  onSuccess: () => void;
}

const FundAccountModal: FC<FundAccountModalProps> = ({ accountId, isOpen, closeModal, onSuccess }) => {
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({
    amount: 0,
  });

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const payload = { accountId, ...values };
    console.log(payload);

    const { error, data } = await fundAccount(payload);

    if (error) {
      alert(error);
      return;
    }

    closeModal();
    onSuccess();
  }

  const handleAmountInputChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      amount: event.target.value,
    }));
  };

  return (
    <>
      <Modal isOpen={isOpen} setIsOpen={closeModal} title="Fund your account">
          <form onSubmit={handleSubmit}>
              <div className="mb-4">
                  <label
                      htmlFor="amount"
                      className="text-gray-500"
                  >
                      Enter the amount you want to transfer
                  </label>
                  <input
                      id="amount"
                      type="text"
                      className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
                      required
                      value={values.amount}
                      onChange={handleAmountInputChange}
                  />
              </div>

              <XButton
                type="submit"
                loading={loading}
                label="Continue"
              />
          </form>
      </Modal>
    </>
  )
}

export default FundAccountModal
