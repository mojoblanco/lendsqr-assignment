import { FC, useState } from "react"
import { transferFunds } from "../../helpers/apiHelper";
import Modal from "../Modal"
import { XButton } from "../ui";

interface TransferFundsProps {
  isOpen: boolean;
  closeModal: () => void;
  accountId: number;
  onSuccess: () => void;
}

const TransferFundsModal: FC<TransferFundsProps> = ({ isOpen, closeModal, accountId, onSuccess }) => {
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({
    amount: 0,
    email: ''
  });

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const payload = { accountId, ...values };
    console.log(payload);

    setLoading(true);
    const { error, data } = await transferFunds(payload);
    setLoading(false);

    if (error) {
      alert(error);
      return;
    }

    closeModal();
    onSuccess();
  }

  const handleAmountInputChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      amount: event.target.value,
    }));
  };

  const handleEmailInputChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      email: event.target.value,
    }));
  };

  return (
    <>
      <Modal isOpen={isOpen} setIsOpen={closeModal} title="Make a transfer">
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              htmlFor="amount"
              className="text-gray-500"
            >
              Enter receipient's email address
            </label>
            <input
              id="email"
              type="email"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
              value={values.email}
              onChange={handleEmailInputChange}
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="amount"
              className="text-gray-500"
            >
              Enter the amount you want to transfer
            </label>
            <input
              id="amount"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
              value={values.amount}
              onChange={handleAmountInputChange}
            />
          </div>

          <XButton
              type="submit"
              loading={loading}
              label="Continue"
            />
        </form>
      </Modal>
    </>
  )
}

export default TransferFundsModal
