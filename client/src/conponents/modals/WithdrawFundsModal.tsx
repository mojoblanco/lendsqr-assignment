import { FC, useState } from "react"
import { withdrawFunds } from "../../helpers/apiHelper";
import Modal from "../Modal"
import { XButton } from "../ui";

interface WithdrawFundsProps {
  isOpen: boolean;
  closeModal: () => void;
  accountId: number;
  onSuccess: () => void;
}

const banks = [
  { name: "United Bank for Africa", id: "uba" },
  { name: "Guaranty Trust Bank", id: "gt-bank" },
  { name: "Wema Bank", id: "wema-bank" },
]

const WithdrawFundsModal: FC<WithdrawFundsProps> = ({ isOpen, closeModal, accountId, onSuccess }) => {
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({
    amount: 0,
    accountNumber: ''
  });

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const payload = { accountId, ...values };
    console.log(payload);

    setLoading(true);
    const { error, data } = await withdrawFunds(payload);
    setLoading(false);

    if (error) {
      alert(error);
      return;
    }

    closeModal();
    onSuccess();
  }

  const handleAmountInputChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      amount: event.target.value,
    }));
  };

  const handleAccountNumberInputChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      accountNumber: event.target.value,
    }));
  };

  return (
    <>
      <Modal isOpen={isOpen} setIsOpen={closeModal} title="Withdraw funds">
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              htmlFor="amount"
              className="text-gray-500"
            >
              Enter the amount you want to withdraw
            </label>
            <input
              id="amount"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
              value={values.amount}
              onChange={handleAmountInputChange}
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="account_number"
              className="text-gray-500"
            >
              Enter your account number
            </label>
            <input
              id="account_number"
              type="text"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2"
              required
              value={values.accountNumber}
              onChange={handleAccountNumberInputChange}
            />
          </div>

          <div className="mb-4">
            <label
              htmlFor="bank"
              className="text-gray-500"
            >
              Select your bank
            </label>
            <select
              name="bank"
              id="bank"
              className="w-full border-b border-gray-300 outline-0 pb-2 my-2 bg-transparent"
            >
              <option value="">- Pick a bank -</option>
              {banks.map(
                bank => <option key={bank.id} value={bank.id}>{bank.name}</option>
              )}
            </select>
          </div>

          <XButton
              type="submit"
              loading={loading}
              label="Continue"
            />
        </form>
      </Modal>
    </>
  )
}

export default WithdrawFundsModal
