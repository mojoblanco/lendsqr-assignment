import { Fragment } from 'react';
import { Dialog, Transition } from '@headlessui/react';

type ModalProps = {
  children: React.ReactNode;
	isOpen: boolean
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
  contentWidth?: string
  title?: string
}

const AppModal = ({
  children,
  isOpen,
  setIsOpen,
  contentWidth = '611px',
  title
}: ModalProps) => {
  return (
    <Dialog
      open={isOpen}
      onClose={() => setIsOpen(false)}
      className="fixed z-10 inset-0 overflow-y-auto"
    >
      <div className="flex items-center justify-center min-h-screen">
        <Dialog.Overlay className="fixed inset-0 bg-black opacity-30" />

        <div className="relative max-w-sm mx-auto w-full">
          <div className='flex justify-end'>
            <button
              className='flex justify-center bg-white rounded-full h-8 w-8 mb-2' onClick={() => setIsOpen(false)}
            >
              <span className='text-xl'>x</span>
            </button>
          </div>

          <div className="bg-white rounded p-6 w-full">
            <h3 className='mb-4 font-bold text-lg'>
              {title}
            </h3>

            {children}
          </div>
        </div>
      </div>
    </Dialog>
  );
}

export default AppModal
