import './App.css'
import { Route, Routes } from 'react-router-dom'
import { Home, Login, Register } from './pages'
import Layout from './conponents/Layout'
import React from "react";


function App() {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/home" element={<Home />} />
      </Routes>
    </Layout>
  )
}

export default App
