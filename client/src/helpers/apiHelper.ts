import axios from 'axios';

const BASE_URL = 'http://localhost:3001';

interface ApiResponse {
    error: string;
    data: any;
}

export const getSession = async () => {
    try {
        const { data } = await axios.get(`${BASE_URL}/session`);

        return { error: null, data };
    } catch(e: any) {
        return { error: e.response.data.message, data: null };
    }
}

export const fundAccount = async (payload: any) => {
    try {
        const { data } = await axios.post(`${BASE_URL}/fund`, payload);

        return { error: null, data };
    } catch(e: any) {
        return { error: e.response.data.message, data: null };
    }
}

export const transferFunds = async (payload: any) => {
    try {
        const { data } = await axios.post(`${BASE_URL}/transfer`, payload);

        return { error: null, data };
    } catch(e: any) {
        return { error: e.response.data.message, data: null };
    }
}

export const withdrawFunds = async (payload: any) => {
    try {
        const { data } = await axios.post(`${BASE_URL}/withdraw`, payload);

        return { error: null, data };
    } catch(e: any) {
        return { error: e.response.data.message, data: null };
    }
}
