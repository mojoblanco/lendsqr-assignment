# Lendsqr assignment

## Requirements
NodeJs v16

## Getting started
- Clone the repository to your local folder

### Set up backend
- Enter the `server` folder and run `npm install`
- Make a copy of the `.env.example` file to `.env` file
- Create a database in your mysql server and update the db credentials in the `.env` files
- Run the command `npm run migrate` to update your database
- Run the command `npm run seed` to seed data into your database
- Run the command `npm run dev` to start the development server

### Set up frontend
- Go to the `client` folder and run `npm install`
- Run the command `npm run dev` to start the application
- Open your favourite browser and go to `http://localhost:3000`
- Sign in to the application

## More info
- API documentation can be accessed by visiting `http://localhost:3001/api-docs`
- When you log in, the first user created in the db is automaticall signed in. All actions performed will be on this user's account